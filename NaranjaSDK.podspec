Pod::Spec.new do |s|
  s.name             = 'NaranjaSDK'
  s.version          = '1.0.2'
  s.summary          = 'Naranja SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-naranjasdk.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.public_header_files = "NaranjaSDK.framework/Headers/*.h"
  s.source_files = "NaranjaSDK.framework/Headers/*.h"
  s.vendored_frameworks = "NaranjaSDK.framework"
  
  s.dependency 'Transactions', '~> 1.0.0'
  s.swift_versions = '5.0'
end
