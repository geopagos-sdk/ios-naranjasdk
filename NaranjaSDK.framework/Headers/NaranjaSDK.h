//
//  NaranjaSDK.h
//  NaranjaSDK
//
//  Copyright © 2020 GeoPagos. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for NaranjaSDK.
FOUNDATION_EXPORT double NaranjaSDKVersionNumber;

//! Project version string for NaranjaSDK.
FOUNDATION_EXPORT const unsigned char NaranjaSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NaranjaSDK/PublicHeader.h>
